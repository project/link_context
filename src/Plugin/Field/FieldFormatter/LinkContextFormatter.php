<?php

/**
 * @file
 * Contains \Drupal\link_context\Plugin\Field\FieldFormatter\LinkContextFormatter.
 */

namespace Drupal\link_context\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\link\Plugin\Field\FieldFormatter\LinkFormatter;

/**
 * Plugin implementation of the 'link_context' formatter.
 *
 * @FieldFormatter(
 *   id = "link_context",
 *   label = @Translation("Link with Context"),
 *   field_types = {
 *     "link"
 *   }
 * )
 */
class LinkContextFormatter extends LinkFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $settings = parent::defaultSettings();
    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);
    foreach ($items as $delta => $item) {
      if (isset($elements[$delta]['#type'])) {
        $elements[$delta]['#type'] = 'link_context';
      }
    }
    return $elements;
  }
}