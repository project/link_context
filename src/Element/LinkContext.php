<?php

/**
 * @file
 * Contains \Drupal\link_context\Element\LinkContext.
 */

namespace Drupal\link_context\Element;

use Drupal\Core\Render\Element\Link;

/**
 * Provides a link_context render element with prefix and suffix.
 *
 * @RenderElement("link_context")
 */
class LinkContext extends Link {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#pre_render' => [
        [$class, 'preRenderLink'],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function preRenderLink($element) {
    $prefix = $suffix = NULL;
    if (array_key_exists('attributes', $element['#options'])) {
      if (array_key_exists('prefix', $element['#options']['attributes'])
        && strlen($element['#options']['attributes']['prefix']) > 0) {
        $prefix = $element['#options']['attributes']['prefix'];
        unset($element['attributes']['prefix']);
      }
      if (array_key_exists('suffix', $element['#options']['attributes'])
        && strlen($element['#options']['attributes']['suffix']) > 0) {
        $suffix = $element['#options']['attributes']['suffix'];
        unset($element['attributes']['suffix']);
      }
    }
    $element = parent::preRenderLink($element);

    if (!empty($element['#markup'])) {
      if (!is_null($prefix)) {
        $element['#markup'] = '<span class="prefix">' . $prefix . '</span>' . LinkContext::spacerIfRequired($prefix, TRUE) . $element['#markup'];
      }
      if (!is_null($suffix)) {
        $element['#markup'] = $element['#markup'] . LinkContext::spacerIfRequired($suffix) . '<span class="suffix">' . $suffix . '</span>';
      }
    }
    return $element;
  }

  public static function spacerIfRequired($text, $end = FALSE) {
    $sequence = $end ? -1 : 0;
    $checking = $end ? '/[A-Za-z0-9).\]:]/' : '/[A-Za-z0-9(\[]/';
    $character = substr($text, $sequence, 1);
    return (preg_match($checking, $character) ? " " : "");
  }
}