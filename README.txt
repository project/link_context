CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

Link Context is an add-on for "Link Attributes widget" module.

Link Attributes widget provides means to add custom attributes to links in link fields.
Link Context makes use of that feature by adding two attributes "Prefix" and "Suffix"
that allows you to add exactly that: a prefix and a suffix for your link.


REQUIREMENTS
------------

  * This module requires Link module from Drupal core.
  * Link Attributes Widget (https://www.drupal.org/project/link_attributes) 


INSTALLATION
------------

 * Install the Link Context module as you would normally install a contributed
   Drupal module. Visit https://www.drupal.org/node/1897420 for further
   information.


CONFIGURATION
-------------

 * After enabling, Link Context will offer two additional fields in the Link
   attributes widget.

MAINTAINERS
-----------

 * Paul Broon - https://www.drupal.org/u/paul-broon